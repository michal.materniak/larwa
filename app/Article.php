<?php

namespace App;

use App\Application\Subpage\Interfaces\SubpageInterface;
use App\Application\Subpage\Traits\SubpageTrait;
use Illuminate\Database\Eloquent\Model;

class Article extends Model implements SubpageInterface
{
    use SubpageTrait;

    protected $table = 'articles';
    protected $primaryKey = 'id_article';
    public const SETTING_GLOBAL = 'global_setting_articles';

    protected $fillable = array(
        'title', 'subpage', 'content', 'slug', 'active'
    );
    public function author()
    {
        return $this->belongsTo(User::class, "author_id");
    }
    public function category()
    {
        return $this->belongsTo(Category::class, "category_id");
    }
    public function comments()
    {
        return $this->hasMany(Comment::class, 'article_id')->orderBy('created_at', 'desc');
    }

    public function getName(): string
    {
        return $this->title;
    }

    public function getNameCategory()
    {
        if(isset($this->category->name))
            return $this->category->name;
        else
            "";
    }

    public function getFirstname(): string
    {
        return $this->author->firstname;
    }
    public function getLastname(): string
    {
        return $this->author->lastname;
    }

    public function shortContent()
    {
        if($this->content != null)
        {
            return substr($this->content, 0, 300);
        }
        else
        {
            return "";
        }
    }
}
