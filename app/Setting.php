<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = array(
        'setting'
    );



    public static function getPrimaryKey()
    {
        return "id_setting";
    }
    private function createSettingTable()
    {
        $this->setting = json_decode($this->setting, true);
    }

    private function saveSettingTable()
    {
        $this->setting = json_encode($this->setting);
    }
    public function getSetting()
    {
        if(!is_array($this->setting)) $this->createSettingTable();

        return $this->setting;
    }
    public function getSettingKey($key)
    {
        if(!is_array($this->setting)) $this->createSettingTable();

        if($this->setting != null && isset($this->setting[$key]))
        {
            return $this->setting[$key];
        }
        else
        {
            return null;
        }
    }
    public function getSettingValueKey($key1, $key2)
    {
        if(!is_array($this->setting)) $this->createSettingTable();

        if($this->setting != null && isset($this->setting[$key1][$key2]))
        {
            return $this->setting[$key1][$key2];
        }
        else
        {
            return null;
        }
    }
}
