<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'comments';
    protected $primaryKey = 'id_comment';

    public function author()
    {
        return $this->belongsTo(User::class, "author_id");
    }
    public function article()
    {
        return $this->belongsTo(Article::class, "article_id");
    }
    public function daysAgo()
    {

        $datetime = new \DateTime($this->created_at);
        return $datetime->diff(new \DateTime())->days;
    }
}
