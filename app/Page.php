<?php

namespace App;

use App\Application\Subpage\Interfaces\SubpageInterface;
use App\Application\Subpage\Traits\SubpageTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model implements SubpageInterface
{

    use SubpageTrait;

    protected $table = 'pages';
    protected $primaryKey = 'id_page';
    public const SETTING_GLOBAL = 'global_setting_pages';

    protected $fillable = array(
        'name', 'subpage', 'content', 'slug', 'active'
    );

    public function getName() : string
    {
        return $this->name;
    }
}