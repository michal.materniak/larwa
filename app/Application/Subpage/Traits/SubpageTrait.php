<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 23.09.18
 * Time: 17:42
 */

namespace App\Application\Subpage\Traits;


use App\Application\Subpage\SubpageFunction;

trait SubpageTrait
{

    private function createSubpageTable()
    {
        $this->subpage = json_decode($this->subpage, true);
    }

    private function saveSubpageTable()
    {
        $this->subpage = json_encode($this->subpage);
    }

    public function getSubpageKey(string $key) : ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage[$key]))
        {
            return $this->subpage[$key];
        }
        else
        {
            return null;
        }
    }
    public function setSubpageKey(string $key, $value)
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();
        $this->subpage[$key] = $value;
        $this->saveSubpageTable();
    }

    public function getTitle(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['title']))
        {
            return $this->subpage['title'];
        }
        else
        {
            return null;
        }
    }

    public function getMetaValue(string $key): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['meta'][$key]))
        {
            return $this->subpage['meta'][$key];
        }
        else
        {
            return null;
        }
    }

    public function getDescription(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['description']))
        {
            return $this->subpage['description'];
        }
        else
        {
            return null;
        }
    }

    public function getHeader(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['header']))
        {
            return $this->subpage['header'];
        }
        else
        {
            return null;
        }
    }

    public function getRedirect(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['redirect']))
        {
            return SubpageFunction::checkURL($this->subpage['redirect']);
        }
        else
        {
            return null;
        }
    }

    public function getCanonical(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['canonical']))
        {

            return SubpageFunction::checkURL($this->subpage['canonical']);
        }
        else
        {
            return null;
        }
    }

    public function getSubpageValue(string $key): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage[$key]))
        {
            return $this->subpage[$key];
        }
        else
        {
            return null;
        }
    }

    public function getSubpage(): array
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        return $this->subpage;
    }

    public function getMeta(): ?string
    {
        if(!is_array($this->subpage)) $this->createSubpageTable();

        if($this->subpage != null && isset($this->subpage['meta']))
        {

            return $this->subpage['meta'];
        }
        else
        {
            return null;
        }
    }

}