<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 21.09.18
 * Time: 23:50
 */

namespace App\Application\Subpage;


class SubpageFunction
{
    public static function createSlug($text) : string
    {

        $delimiter = '-';


        $unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
            'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z'];
        $text = strtr( $text, $unwanted_array );


        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $text))))), $delimiter));
        return $slug;
    }
    public static function checkURL($url) : ?string
    {
        $parsedURL = parse_url($url);

        if(isset($parsedURL['host']))
        {
            $path = (isset($parsedURL['path'])) ? $parsedURL['path'] : "/";
            $query = (isset($parsedURL['query'])) ? "?" . $parsedURL['query'] : "";

            return $parsedURL['scheme'] . "://". $parsedURL['host'] . $path . $query;
        }
        else
        {
            return null;
        }
    }
    public static function parseRequestData(array $request) : array
    {
        $request_return = array();
        foreach ($request as $oneRow_k => $oneRow_v)
        {
            $key = explode('-', $oneRow_k);
            if(count($key) == 1)
            {
                $request_return[$key[0]] = $oneRow_v;
            }
            if(count($key) == 2)
            {
                $request_return[$key[0]][$key[1]] = $oneRow_v;
            }
            if(count($key) == 3)
            {
                $request_return[$key[0]][$key[1]][$key[2]] = $oneRow_v;
            }
            else
            {
                continue;
            }
        }

        foreach ($request_return as $oneRow_k => $oneRow_v )
        {
            if(is_array($oneRow_v))
            {
                $request_return[$oneRow_k] = json_encode($oneRow_v);
            }
        }

        return $request_return;
    }
}