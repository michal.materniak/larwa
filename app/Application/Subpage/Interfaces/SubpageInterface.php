<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 21.09.18
 * Time: 23:44
 */

namespace App\Application\Subpage\Interfaces;


interface SubpageInterface
{
    /**
     * @return string|null
     */
    public function getName(): string;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @return string|null
     */
    public function getMetaValue(string $key): ?string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @return string|null
     */
    public function getHeader(): ?string;

    /**
     * @return string|null
     */
    public function getSubpageValue(string $key): ?string;

    /**
     * @return array
     */
    public function getSubpage(): array;

    /**
     * @return string|null
     */
    public function getRedirect(): ?string;

    /**
     * @return string|null
     */
    public function getCanonical(): ?string;

}