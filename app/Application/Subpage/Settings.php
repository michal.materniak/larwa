<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 08.04.18
 * Time: 14:29
 */
namespace App\Application\Settings;


use App\Application\Subpage\Interfaces\SubpageInterface;
use App\Page;
use App\Setting;


class Settings
{
    private static $instance;
    private $settings;
    protected $parsedSettings = array();
    protected $subpage;
    /**
     * @var Setting $settingGlobal
     */
    protected $settingGlobal;
    public const SETTING_GLOBAL_NAME = 'global_settings';
    private function __construct(array $settings, SubpageInterface $subpage)
    {
        $this->settingGlobal = Setting::where('id_setting', self::SETTING_GLOBAL_NAME)->first();
        $this->subpage = $subpage;
        $this->settings = $settings;
    }
    public static function getInstance(string $settingsKey = "", SubpageInterface $subpage = null)
    {
        if(self::$instance == null && ($settingsKey != null && $subpage != null))
        {

            $setting = Setting::where(Setting::getPrimaryKey(), $settingsKey)->first();
            self::$instance = new static($setting->getSetting(), $subpage);
        }
        return self::$instance;
    }
    private function getFromObject(object $object, $function)
    {

        return call_user_func(array($object, $function));
    }
    private function parseSettings()
    {
        $this->parsedSettings = array();
        foreach ($this->settings as $setting_k => $setting_v)
        {
            $matches = null;
            $value = $setting_v;
            preg_match_all("/({!)(.*?)(!})/", $setting_v, $matches);

            if(isset($matches[0]) != null)
            {
                foreach ($matches[0] as $match)
                {
                    $value = str_replace($match, "%s", $value);
                }
            }
            $this->parsedSettings[$setting_k] = array(
                $value, $matches[2]
            );
        }
    }
    private function dateV($format,$timestamp=null){
        $to_convert = array(
            'l'=>array('dat'=>'N','str'=>array('Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota','Niedziela')),
            'D'=>array('dat'=>'N','str'=>array('Pon','Wt','Śr','Czw','Pt','Sob','Nie')),
            'F'=>array('dat'=>'n','str'=>array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień')),
            'f'=>array('dat'=>'n','str'=>array('stycznia','lutego','marca','kwietnia','maja','czerwca','lipca','sierpnia','września','października','listopada','grudnia')),
            'M'=>array('dat'=>'n','str'=>array('Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru'))
        );
        if ($pieces = preg_split('#[:/.\-, ]#', $format)){
            if ($timestamp === null) { $timestamp = time(); }
            foreach ($pieces as $datepart){
                if (array_key_exists($datepart,$to_convert)){
                    $replace[] = $to_convert[$datepart]['str'][(date($to_convert[$datepart]['dat'],$timestamp)-1)];
                }else{
                    $replace[] = date($datepart,$timestamp);
                }
            }
            $result = strtr($format,array_combine($pieces,$replace));
            return $result;
        }
    }
    private function fromGlobal($nameKey)
    {
        $nameKey_array = explode(':', $nameKey);
        if(count($nameKey_array) > 1)
        {
            switch ($nameKey_array[0])
            {
                case 'currentDate':
                    {
                        return $this->dateV($nameKey_array[1]);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
        else
        {
            return $this->settingGlobal->getSettingKey($nameKey);
        }
    }
    private function parseSettingsSEO()
    {

        $this->parseSettings();

        foreach ($this->parsedSettings as $parsedSetting_k => &$parsedSetting_v)
        {
            foreach ($parsedSetting_v[1] as &$match)
            {

                try
                {

                    $functions = explode('.', $match);
                    if($functions[0] != 'global')
                    {

                        $object_temp = $this->subpage;
                        $value = null;
                        foreach ($functions as $name_function)
                        {
                            $function_array = explode(':', $name_function);
                            if(count($function_array) > 1)
                            {
                                $name_function = $function_array[0];
                                $value_function = $function_array[1];

                                $function =  'get' . $name_function;
                                try
                                {
                                    $value = $this->getFromObject($object_temp, $function)[$value_function];
                                }
                                catch (\ErrorException $ex)
                                {
                                    $value = null;
                                }
                            }
                            else{

                                $function =  'get' . $name_function;
                                $value = $this->getFromObject($object_temp, $function);
                                $object_temp = $value;
                            }
                        }
                    }
                    else
                    {
                        $value = $this->fromGlobal($functions[1]);
                    }
                }
                catch (\ErrorException $ex)
                {
                    $value = "";
                }

                $match = $value;
            }
            $parsedSetting_v[0] = vsprintf($parsedSetting_v[0], $parsedSetting_v[1]);
            $this->parsedSettings[$parsedSetting_k] = $parsedSetting_v[0];
        }
    }
    public function getSettingsSEO() : array
    {
    // jezeli w ustawieniach podstrony istnieje title, descriptioon lub inne, to nadpisuje, w przeciwnym wypadku, generuje.
        $this->parseSettingsSEO();
        $settings = array();

        foreach ($this->parsedSettings as $setting_parsed_k => $setting_parsed_v)
        {

            $tryMethod = 'get' . ucfirst( $setting_parsed_k );

            if(method_exists($this->subpage, $tryMethod))
            {
                $value = call_user_func(array($this->subpage, $tryMethod));
                if($value != null)
                {
                    $settings[$setting_parsed_k] = $value;
                }
                else
                {
                    $settings[$setting_parsed_k] = $setting_parsed_v;
                }
            }
            elseif (($value = call_user_func_array(array($this->subpage, "getMetaValue"), array($setting_parsed_k))) != null)
            {
                $settings[$setting_parsed_k] = $value;
            }
            else
            {
                $settings[$setting_parsed_k] = $setting_parsed_v;
            }
        }
        return $settings;
    }
}