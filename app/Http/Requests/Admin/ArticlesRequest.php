<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ArticlesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
//            'category' => 'required',
        ];
    }
    /**
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => "Wpisz tytuł",
//            'category.required' => "Wybierz kategorię"
        ];
    }
}
