<?php

namespace App\Http\Controllers\Page;

use App\Application\Settings\Settings;
use App\Http\Controllers\Controller;
use App\Page;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomepageController extends GlobalController
{
    public function homepage()
    {
        $setting = Setting::where('id_setting', 'global_settings')->first();
        $this->blade_array['page'] = $page = Page::where('id_page', $setting->getSettingKey('homepage'))->first();
        $this->blade_array['settingsSeo'] = $settingsSeo = Settings::getInstance(Page::SETTING_GLOBAL, $page)->getSettingsSEO();
        return view('page.homepage.homepage', $this->blade_array);
    }
}
