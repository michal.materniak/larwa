<?php

namespace App\Http\Controllers\Page;

use App\Application\Settings\Settings;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PagesController extends GlobalController
{

    public function page($page_slug)
    {
        $this->blade_array['page'] = $page = Page::where('slug', $page_slug)->first();
        if($page)
        {
            if($page->getRedirect() != null) return redirect($page->getRedirect(), 301);


            $this->blade_array['settingsSeo'] = $settingsSeo = Settings::getInstance(Page::SETTING_GLOBAL, $page)->getSettingsSEO();

            return view('page.pages.page', $this->blade_array);
        }
        else
        {
            abort(404);
        }

    }
}