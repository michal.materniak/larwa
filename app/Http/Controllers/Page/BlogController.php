<?php

namespace App\Http\Controllers\Page;

use App\Application\Settings\Settings;
use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends GlobalController
{

    public function article($slug)
    {

        $this->blade_array['article'] = $article = Article::where('slug', $slug)->with('category')->with('author')->first();
        if($article && $article->active == true)
        {
            if($article->getRedirect() != null)
                if($article->getRedirect() != null) return redirect($article->getRedirect(), 301);

            $this->blade_array['settingsSeo'] = $settingsSeo = Settings::getInstance(Article::SETTING_GLOBAL, $article)->getSettingsSEO();
            return view('page.blog.article', $this->blade_array);
        }
        else
        {
            abort(404);
        }
    }

    public function list()
    {
        $this->blade_array['articles'] = $articles = Article::where('active', true)->with('author')->with('category')->orderBy('id_article', 'desc')->paginate(5);

        return view('page.blog.list', $this->blade_array);
    }
    public function category($slug)
    {
        $category = Category::where('slug', $slug)->first();
        $this->blade_array['articles'] = $articles = Article::where([['active', true], ['category_id', $category->id_category]] )->with('author')->with('category')->orderBy('id_article', 'desc')->paginate(5);

        return view('page.blog.list', $this->blade_array);
    }
}