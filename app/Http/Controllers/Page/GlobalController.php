<?php

namespace App\Http\Controllers\Page;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Setting;
use Illuminate\Http\Request;

abstract class GlobalController extends Controller
{
    protected $blade_array;
    public function __construct()
    {
        $this->blade_array['menuPages'] = Page::where('active', true)->get();
        $this->blade_array['globalSettings'] = Setting::where('id_setting', 'global_settings')->first()->getSetting();
        $this->blade_array['categories'] = Category::all();
    }
}