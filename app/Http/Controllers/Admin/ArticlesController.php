<?php

namespace App\Http\Controllers\Admin;

use App\Application\Settings\Settings;
use App\Application\Subpage\SubpageFunction;
use App\Article;
use App\Category;
use App\Http\Requests\Admin\ArticlesRequest;
use App\Http\Requests\Admin\PagesRequest;
use App\Page;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Psy\Exception\ErrorException;

class ArticlesController extends GlobalController
{
    public function update(ArticlesRequest $request, Article $article)
    {

        try
        {
            if(
            (Auth::user()->hasRole('User') && Auth::id() != $article->author_id)
            ) throw new ErrorException();

            if($article)
            {
                    $this->blade_array['article'] = $article;
                    $article->category()->associate(Category::where('id_category', $request->input('category_id'))->first());
                    $article->update(SubpageFunction::parseRequestData($request->all()));
                    $this->blade_array['settingSubpage'] = Settings::getInstance(Article::SETTING_GLOBAL, $article)->getSettingsSEO();

                    return view('admin.articles.article', $this->blade_array);

            }
            else
            {
                throw new ErrorException();
            }
        }
        catch ( \Illuminate\Database\QueryException | ErrorException $ex)
        {
            return redirect(route('admin-homepage'));
        }
    }
    public function create(ArticlesRequest $request)
    {
        try
        {

            $article = new Article();
            $article->title = $request->input('title');

            $article->slug = SubpageFunction::createSlug($article->title);
            $article->active = true;
            $article->author()->associate(Auth::id());

            $article->save();
            $this->blade_array['article'] = $article;
            $this->blade_array['settingSubpage'] = Settings::getInstance(Article::SETTING_GLOBAL, $article)->getSettingsSEO();

            return view('admin.articles.article', $this->blade_array);
        }
        catch ( \Illuminate\Database\QueryException $ex)
        {

            return redirect(route('admin-homepage'));
        }
    }

    public function article(Article $article)
    {
        if($article)
        {

            try
            {

                if(
                    (Auth::user()->hasRole('User') && Auth::id() != $article->author_id)
                ) throw new ErrorException();

                $this->blade_array['article'] = $article;

                $this->blade_array['settingSubpage'] = Settings::getInstance(Article::SETTING_GLOBAL, $article)->getSettingsSEO();

                return view('admin.articles.article', $this->blade_array);
            }
            catch ( \Illuminate\Database\QueryException | ErrorException $ex)
            {

                return redirect(route('admin-homepage'));
            }
        }
        else
        {
            throw new ErrorException();
        }

    }

    public function active()
    {
        //$pages = DB::table('pages')->leftJoin('subpages', 'pages.id_subpage', '=', 'subpages.id_subpage' )->where('active', true)->get();
        if(Auth::user()->hasRole("Admin"))
        {
            $this->blade_array['articles'] = $articles = Article::where(['active'=> true])->with('author')->orderBy('id_article', 'desc')->get();
        }
        else
        {
            $this->blade_array['articles'] = $articles = Article::where(['active'=> true, 'author_id' => Auth::id()])->with('author')->orderBy('id_article', 'desc')->get();

        }
        return view('admin.articles.active', $this->blade_array);
    }

    public function unactive()
    {
        if(Auth::user()->hasRole("Admin"))
        {
            $this->blade_array['articles'] = $articles = Article::where(['active'=> false])->with('author')->orderBy('id_article', 'desc')->get();
        }
        else
        {
            $this->blade_array['articles'] = $articles = Article::where(['active'=> false, 'author_id' => Auth::id()])->with('author')->orderBy('id_article', 'desc')->get();

        }        return view('admin.articles.unactive', $this->blade_array);
    }

}
