<?php

namespace App\Http\Controllers\Admin;

use App\Application\Settings\Settings;
use App\Application\Subpage\SubpageFunction;
use App\Article;
use App\Http\Requests\Admin\PagesRequest;
use App\Page;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Psy\Exception\ErrorException;

class PagesController extends GlobalController
{
    public function update(PagesRequest $request, Page $id_page)
    {
        if($id_page)
        {
            try
            {
                $this->blade_array['page'] = $id_page;
                $id_page->update(SubpageFunction::parseRequestData($request->all()));
                $this->blade_array['settingSubpage'] = Settings::getInstance(Page::SETTING_GLOBAL, $id_page)->getSettingsSEO();

                return view('admin.pages.page', $this->blade_array);
            }
            catch ( \Illuminate\Database\QueryException $ex)
            {
                return redirect(route('admin-homepage'));
            }
        }
        else
        {
            throw new ErrorException();
        }

    }

    public function page(Page $id_page)
    {
        if($id_page)
        {
            try
            {
                $this->blade_array['page'] = $id_page;
                $this->blade_array['settingSubpage'] = $setting = Settings::getInstance(Page::SETTING_GLOBAL, $id_page)->getSettingsSEO();

                return view('admin.pages.page', $this->blade_array);
            }
            catch ( \Illuminate\Database\QueryException $ex)
            {
                return redirect(route('admin-homepage'));
            }
        }
        else
        {
            throw new ErrorException();
        }
    }
    public function menu()
    {
        //$pages = DB::table('pages')->leftJoin('subpages', 'pages.id_subpage', '=', 'subpages.id_subpage' )->where('active', true)->get();
        $this->blade_array['pages'] = $pages = Page::where('active', true)->orderBy('id_page', 'desc')->get();
        return view('admin.pages.menu', $this->blade_array);
    }

    public function other()
    {
        $this->blade_array['pages'] = $pages = Page::where('active', false)->orderBy('id_page', 'desc')->get();
        return view('admin.pages.other', $this->blade_array);
    }
    public function create(PagesRequest $request)
    {
        try
        {
            $page = new Page();
            $page->name = $request->input('name');
            $page->slug = SubpageFunction::createSlug($page->name);
            $page->active = true;

            $page->save();
            $this->blade_array['page'] = $page;
            $this->blade_array['settingSubpage'] = Settings::getInstance(Article::SETTING_GLOBAL, $page)->getSettingsSEO();

            return view('admin.pages.page', $this->blade_array);
        }
        catch ( \Illuminate\Database\QueryException $ex)
        {

            return redirect(route('admin-homepage'));
        }
    }

}
