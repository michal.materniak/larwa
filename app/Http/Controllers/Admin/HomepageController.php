<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends GlobalController
{
    public function homepage()
    {
        return view('admin.homepage.homepage');
    }
}
