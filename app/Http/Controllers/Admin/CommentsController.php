<?php

namespace App\Http\Controllers\Admin;

use App\Application\Settings\Settings;
use App\Application\Subpage\SubpageFunction;
use App\Article;
use App\Comment;
use App\Http\Requests\Admin\ArticlesRequest;
use App\Http\Requests\Admin\CommentsRequest;
use App\Http\Requests\Admin\PagesRequest;
use App\Page;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Psy\Exception\ErrorException;

class CommentsController extends GlobalController
{

    public function show()
    {
        $this->blade_array['comments']  = Comment::with('author')->orderBy('created_at', 'desc')->with('article')->paginate(10);
        return view('admin.comments.show', $this->blade_array);
    }
    public function own()
    {
        $this->blade_array['comments']  = Comment::where('author_id', Auth::id())->with('author')->with('article')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.comments.own', $this->blade_array);
    }
    public function create(CommentsRequest $request, Article $article)
    {
        $comment = new Comment();
        $comment->content = $request->input('content');
        $comment->article()->associate($article);
        $comment->author()->associate(Auth::user());
        $comment->save();
        return redirect(route('page-articles-article', ['slug' => $article->slug]));
    }
}
