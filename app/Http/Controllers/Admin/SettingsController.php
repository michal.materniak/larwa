<?php

namespace App\Http\Controllers\Admin;

use App\Application\Subpage\SubpageFunction;
use App\Http\Requests\Admin\SettingsRequest;
use App\Setting;
use Psy\Exception\ErrorException;

class SettingsController extends GlobalController
{
    public function setting(Setting $setting)
    {
        if($setting)
        {
            try
            {
                $this->blade_array['setting'] = $setting;
                return view('admin.settings.setting', $this->blade_array);
            }
            catch ( \Illuminate\Database\QueryException $ex)
            {
                return redirect(route('admin-homepage'));
            }
        }
        else
        {
            throw new ErrorException();
        }
    }
    public function update(SettingsRequest $request, Setting $setting)
    {
        if($setting)
        {
            try
            {
                $this->setting($setting);
                $setting->update(SubpageFunction::parseRequestData($request->all()));
                return view('admin.settings.setting', $this->blade_array);
            }
            catch ( \Illuminate\Database\QueryException $ex)
            {
                return redirect(route('admin-homepage'));
            }
        }
        else
        {
            throw new ErrorException();
        }
    }
}
