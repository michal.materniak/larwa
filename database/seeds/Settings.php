<?php

use Illuminate\Database\Seeder;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $setting = new \App\Setting();
            $setting->id_setting = "global_settings";
            $setting->description = "Ustawienia globalne dla serwisu";
            $setting->setting = "{
  \"meta\": {
    \"bodyEnd\": \"\",
    \"headEnd\": \"\",
    \"bodyBegin\": \"\",
    \"headBegin\": \"\"
  },
  \"name\": \"Test Laravel\",
  \"domain\": \"laravel.loc\",
  \"domain\": \"laravel.loc\",
  \"footer\": {
    \"text\": \"\"
  },
  \"sideWidget\": {
    \"text\": \"Weekendowy kurs Laravela :)\"
  },
  \"homepage\": \"1\"
}";
            $setting->save();

            $setting = new \App\Setting();
            $setting->id_setting = "global_setting_pages";
            $setting->description = "Ustawienia globalne dla zakładek";
            $setting->setting = "{\"title\": \"{!name!} - {!global.name!} \", \"header\": \"{!name!}\", \"description\": \"{!name!}  {!global.domain!}\"}";
            $setting->use = "
                                            <h5 >Zmienne należy uwzględnić w znacznikach {! !} bez spacji </h5>

                                <ul style=\"list-style-type: none;\">
                                    <p>Ustawienia globalne można ustawić w Serwis->Ustawienia </p>
                                    <p><dfn><i>{!global.name!}</i></dfn> nazwa serwisu / brand </p>
                                    <p><dfn><i>{!global.domain!}</i></dfn> domena </p>
                                    <p><dfn><i>{!global.currentDate:xxx!}</i></dfn> aktualna data, przetłumaczona na j. pol - <a href=\"http://php.net/manual/en/function.date.php\" target=\"_blank\">Możliwości</a>
                                        <br>Np:<br>{!global.currentDate:F!} - wyświetli aktualną nazwę miesiąca w mianowniku
                                        <br>{!global.currentDate:f!} - wyświetli aktualną, odmienioną nazwę miesiąca
                                    </p>
                                    <p><dfn><i>{!name!}</i></dfn> nazwa zakładki</p>
                                </ul>
            ";
            $setting->save();

            $setting = new \App\Setting();
            $setting->id_setting = "global_setting_articles";
            $setting->description = "Ustawienia globalne dla artykułów";
            $setting->setting = "{\"title\": \"{!name!} - {!global.name!} \", \"header\": \"{!name!}\", \"description\": \"{!name!}  {!global.domain!}\"}";
            $setting->use = "
                                            <h5 >Zmienne należy uwzględnić w znacznikach {! !} bez spacji </h5>

                                <ul style=\"list-style-type: none;\">
                                    <p>Ustawienia globalne można ustawić w Serwis->Ustawienia </p>
                                    <p><dfn><i>{!global.name!}</i></dfn> nazwa serwisu / brand </p>
                                    <p><dfn><i>{!global.domain!}</i></dfn> domena </p>
                                    <p><dfn><i>{!global.currentDate:xxx!}</i></dfn> aktualna data, przetłumaczona na j. pol - <a href=\"http://php.net/manual/en/function.date.php\" target=\"_blank\">Możliwości</a>
                                        <br>Np:<br>{!global.currentDate:F!} - wyświetli aktualną nazwę miesiąca w mianowniku
                                        <br>{!global.currentDate:f!} - wyświetli aktualną, odmienioną nazwę miesiąca
                                    </p>
                                    <p><dfn><i>{!title!}</i></dfn> tytuł artykułu</p>
                                    <p><dfn><i>{!firstname!}</i></dfn> Imie autora wpisu</p>
                                    <p><dfn><i>{!lastname!}</i></dfn> Nazwisko autora wpisu</p>
                                    <p><dfn><i>{!nameCategory!}</i></dfn> Nazwa Kategori, jeśli jest przypisana do artykułu</p>
                                </ul>
            ";
            $setting->save();
        }
        catch (Illuminate\Database\QueryException $ex)
        {
            echo "nie dodano\n";
        }
    }
}
