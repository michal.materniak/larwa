<?php

use Illuminate\Database\Seeder;
use App\Page as PagesEntity;

class Pages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try
        {
            $page = new PagesEntity();
            $page->name = "Strona Główna";
            $page->slug = \App\Application\Subpage\SubpageFunction::createSlug($page->name);
            $page->active = false;
            $page->content = "Treść na stronie głównej";
            $page->save();


            $page = new PagesEntity();
            $page->name = "O mnie";
            $page->slug = \App\Application\Subpage\SubpageFunction::createSlug($page->name);
            $page->active = true;
            $page->content = "Treść na zakładce o mnie";
            $page->save();


            $page = new PagesEntity();
            $page->name = "Kontakt";
            $page->slug = \App\Application\Subpage\SubpageFunction::createSlug($page->name);
            $page->active = true;
            $page->content = "Treść na zakładce kontakt";
            $page->save();

            $page = new PagesEntity();
            $page->name = "Oferta";
            $page->slug = \App\Application\Subpage\SubpageFunction::createSlug($page->name);
            $page->active = true;
            $page->content = "Treść na zakładce oferta";
            $page->save();


            $page = new PagesEntity();
            $page->name = "Realizacje";
            $page->slug = \App\Application\Subpage\SubpageFunction::createSlug($page->name);
            $page->active = true;
            $page->content = "Treść na zakładce realizacje";
            $page->save();


        }
        catch (\Illuminate\Database\QueryException $ex)
        {
            echo "nie dodano\n";
        }


    }
}
