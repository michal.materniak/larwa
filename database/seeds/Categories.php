<?php

use Illuminate\Database\Seeder;

class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try
        {
            $category = new \App\Category();
            $category->name = "Programowanie";
            $category->slug = \App\Application\Subpage\SubpageFunction::createSlug($category->name);
            $category->save();


            $category = new \App\Category();
            $category->name = "HTML";
            $category->slug = \App\Application\Subpage\SubpageFunction::createSlug($category->name);
            $category->save();


            $category = new \App\Category();
            $category->name = "Java Script";
            $category->slug = \App\Application\Subpage\SubpageFunction::createSlug($category->name);
            $category->save();


            $category = new \App\Category();
            $category->name = "SEO i Marketing";
            $category->slug = \App\Application\Subpage\SubpageFunction::createSlug($category->name);
            $category->save();

            $category = new \App\Category();
            $category->name = "PHP";
            $category->slug = \App\Application\Subpage\SubpageFunction::createSlug($category->name);
            $category->save();

        }
        catch (\Illuminate\Database\QueryException $ex)
        {
            echo "nie dodano\n";
        }

    }
}
