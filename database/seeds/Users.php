<?php

use Illuminate\Database\Seeder;
use App\User as UserEntity;
class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \App\Role::all();
        try
        {
            $user = new UserEntity();
            $user->firstname = "Michał";
            $user->lastname = "Materniak";
            $user->email = "michal.materniak@commit-m.pl";
            $user->remember_token = "nL8UoxQy7mG23MducEoOtf5ATiuOpKqUcYjSBL7zZUrqHqSJ5B7FiNq5vZSh";
            $user->password = bcrypt('admin1');
            $user->save();
            $user->roles()->attach($roles[0]);

            $user = new UserEntity();
            $user->firstname = "Admin";
            $user->lastname = "Adminek";
            $user->email = "admin.adminek@admin.pl";
            $user->remember_token = "nL8UoxQy7mG23MducEoOtf5ATiuOpKqUcYjSBL7zZUrqHqSJ5B7FiNq5vZSh";
            $user->password = bcrypt('admin1');
            $user->save();
            $user->roles()->attach($roles[0]);

            for($i = 0; $i < 100; ++$i)
            {
                $user = new UserEntity();
                $user->firstname = Faker\Factory::create()->firstName;
                $user->lastname = Faker\Factory::create()->lastName;
                $user->email = $user->firstname . '.' . $user->lastname . "@domena.pl";
                $user->remember_token = "nL8UoxQy7mG23MducEoOtf5ATiuOpKqUcYjSBL7zZUrqHqSJ5B7FiNq5vZSh";
                $user->password = bcrypt('admin1');
                $user->save();
                $user->roles()->attach($roles[1]);
            }
        }
        catch (Illuminate\Database\QueryException $ex)
        {
            echo "nie dodano\n";
        }
    }
}
