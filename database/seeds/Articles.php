<?php

use Illuminate\Database\Seeder;
use App\Article as ArticlesEntity;
class Articles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::where('id', 1)->orWhere('id', 2)->get();
        $categories = \App\Category::all();
        for($i = 0; $i < 100; ++$i)
        {
            try
            {
                $article = new ArticlesEntity();
                $article->title = Faker\Factory::create()->text(100);
                $article->content = Faker\Factory::create()->text(5000);
                $article->slug = \App\Application\Subpage\SubpageFunction::createSlug($article->title);

                if(($i % 4) == 0)
                {
                    $article->active = false;
                }
                else
                {
                    $article->active = true;
                }
                $article->author()->associate($users[rand(0, 1)]);
                $category = $categories[rand(0, count($categories)-1)];
                $article->category()->associate($category);
                $article->save();

            }
            catch (\Illuminate\Database\QueryException $ex)
            {
                dump($ex);
                echo "nie dodano\n";
                continue;
            }
        }
    }
}
