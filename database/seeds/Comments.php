<?php

use Illuminate\Database\Seeder;

class Comments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = \App\Article::all();
        $users = \App\User::all();

        for($i = 0; $i < 100; ++$i)
        {
            try
            {
                $comment = new \App\Comment();
                $comment->content = Faker\Factory::create()->text(200);

                $comment->created_at = Faker\Factory::create()->dateTimeBetween($startDate = '-30 days', $endDate = 'now');

                $comment->author()->associate($users[rand(0, count($users)-1)]);

                $comment->article()->associate($articles[rand(0, count($articles)-1)]);
                $comment->save();

            }
            catch (\Illuminate\Database\QueryException $ex)
            {
                echo "nie dodano\n";
                continue;
            }
        }
    }
}
