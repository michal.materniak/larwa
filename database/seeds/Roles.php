<?php

use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try
        {
            $role = new \App\Role();
            $role->name = 'Admin';
            $role->save();

            $role = new \App\Role();
            $role->name = 'User';
            $role->save();

        }
        catch (\Illuminate\Database\QueryException $ex)
        {
            echo "nie dodano\n";
        }

    }
}
