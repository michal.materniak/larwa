<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Articles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id_article');
            $table->string('title');
            $table->longText('content')->nullable(true);
            $table->string('slug')->unique();
            $table->integer('author_id')->unsigned();
            $table->integer('category_id')->unsigned()->nullable(true);
            $table->boolean('active');
            $table->json('subpage')->nullable(true);
            $table->timestamps();
        });
        Schema::table('articles', function (Blueprint $table){
            $table->foreign('author_id')->references('id')->on('users');
        });
        Schema::table('articles', function (Blueprint $table){
            $table->foreign('category_id')->references('id_category')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
