<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id_comment');
            $table->longText('content');
            $table->integer('author_id')->unsigned();
            $table->integer('article_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('comments', function (Blueprint $table){
            $table->foreign('author_id')->references('id')->on('users');
        });
        Schema::table('comments', function (Blueprint $table){
            $table->foreign('article_id')->references('id_article')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
