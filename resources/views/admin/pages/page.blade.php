@extends('admin.base')

@section('title')
{{ $page->name }} - Edytowanie zakładki
@endsection

@section('assets_head')
    <link rel="stylesheet" href="{{ asset('admin/assets/icons/fontawesome/styles.min.css') }}">

    <script type="text/javascript" src="{{ asset('admin/lib/js/jquery.min.js') }}"></script>@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h2 class="card-title">Edytowanie zakładki {{ $page->name }}</h2>

            <div id="message_ajax">

                @if (\App\Application\Subpage\SubpageFunction::checkURL($page->getSubpageKey('redirect')) == null && $page->getSubpageKey('redirect') != null )
                    <div style="color: red;">
                        Ustawiony jest zły adres przekierowania. Popraw adres
                    </div>
                @endif
                @if (\App\Application\Subpage\SubpageFunction::checkURL($page->getSubpageKey('canonical')) == null && $page->getSubpageKey('canonical') != null )
                    <div style="color: red;">
                        Ustawiony jest zły adres canonical. Popraw adres
                    </div>
                @endif
                @if ($errors->any())
                    @foreach($errors->all() as $error)
                        <div style="color: red;">
                            {{ $error }}
                        </div>
                    @endforeach
                @elseif (isset($saved) && $saved == true)
                    <div style="color: green;">
                        Zapisano
                    </div>
                @endif
            </div>
            {!! Form::model($page, ['route' => ['admin-pages-update', $page], 'method' => "PUT"]) !!}

            <div class="card">
                <div class="row sticky-top">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('page-pages', ['page_slug' => $page->slug]) }}" class="btn btn-primary" target="_blank">Przejdz do podstrony</a>
                        {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
                        {{--<button type="submit" class="btn btn-primary" id="create_modify"--}}
                                {{--name="modify">--}}

                        {{--</button>--}}
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title">Meta</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="content-group margin-top-10">
                                <div class="form-group">
                                    {!! Form::label('name', "Nazwa:", null, ['class' => 'form-control']) !!}
                                    {!! Form::text('name', $page->name, ['class'=>'form-control', 'name' => 'name' ]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('active', "Strona Aktywna:", null, ['class' => 'form-control']) !!}
                                    {!! Form::radio('active', '1', $page->active); !!}
                                    {!! Form::label('active', "Strona Nieaktywna:", null, ['class' => 'form-control']) !!}
                                    {!! Form::radio('active', '0', $page->active); !!}
                                </div>
                                <div class="form-group">
                                    <label class="control-label">ID</label>
                                    <input type="text" class="form-control" id="id" value="{{ $page->id_page }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">URL</label>

                                    @if (\App\Application\Subpage\SubpageFunction::checkURL($page->getSubpageKey('redirect')) == null)
                                    <input type="text" class="form-control" value="{{ route('page-pages', ['slug' => $page->slug]) }}" readonly >
                                    @else
                                    <input style="text-decoration: line-through"  type="text" class="form-control" value="{{ route('page-pages', ['slug' => $page->slug]) }}" readonly >
                                    <input type="text" class="form-control" value="{{ \App\Application\Subpage\SubpageFunction::checkURL($page->getSubpageKey('redirect')) }}" readonly >
                                    @endif
                                </div>
                                <div class="form-group" title="Zmiana tytułu nadpisze automatyczne generowanie. By przywrócić do automatycznego generowania, skasuj zawartość i pozostaw pole puste">
                                    {!! Form::label('subpage.title', "Tytuł: ", null, ['class' => 'form-control']) !!}
                                    {!! Form::text('subpage.title', $page->getSubpageKey('title'), ['class'=>'form-control', 'name' => 'subpage-title', 'placeholder' => $settingSubpage['title']] ) !!}
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="content-group margin-top-10">
                                <div class="form-group">
                                    {!! Form::label('subpage.canonical', "Canonical:", null, ['class' => 'form-control']) !!}
                                    {!! Form::text('subpage.canonical', $page->getSubpageKey('canonical'), ['class'=>'form-control', 'name' => 'subpage-canonical' ] ) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('subpage.redirect', "Przekierowanie 301:", null, ['class' => 'form-control']) !!}
                                    {!! Form::text('subpage.redirect', $page->getSubpageKey('redirect'), ['class'=>'form-control', 'name' => 'subpage-redirect'] ) !!}

                                </div>
                                <div class="form-group">
                                    {!! Form::label('subpage.meta', "Inne Meta:", null, ['class' => 'form-control']) !!}
                                    {!! Form::textarea('subpage.meta', $page->getSubpageKey('meta'), ['class'=>'form-control', 'name' => 'subpage-meta' ] ) !!}

                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <legend class="text-bold">Treść</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="content-group margin-top-10">
                                <div class="form-group" title="Zmiana nagłówka nadpisze automatyczne generowanie. By przywrócić do automatycznego generowania, skasuj zawartość i pozostaw pole puste">
                                    {!! Form::label('subpage.header', "Nagłówek H1: " , null, ['class' => 'form-control']) !!}
                                    {!! Form::text('subpage.header', $page->getSubpageKey('header'), ['class'=>'form-control', 'name' => 'subpage-header', 'placeholder' => $settingSubpage['header']] ) !!}
                                </div>
                            </fieldset>
                            {{--<fieldset class="content-group margin-top-10">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Logo</label>--}}
                                    {{--<input type="text" class="form-control" value="" >--}}
                                    {{--<img src="" class="img-thumbnail">--}}
                                {{--</div>--}}
                            {{--</fieldset>--}}
                            <fieldset class="content-group margin-top-10">
                                <div class="form-group" title="Zmiana opisu nadpisze automatyczne generowanie. By przywrócić do automatycznego generowania, skasuj zawartość i pozostaw pole puste">
                                    {!! Form::label('subpage.description', "Meta Opis: ", null, ['class' => 'form-control']) !!}
                                    {!! Form::textarea('subpage.    description', $page->getSubpageKey('description'), ['class'=>'form-control', 'name' => 'subpage-description', 'placeholder' => $settingSubpage['description']] ) !!}

                                </div>

                            </fieldset>
                            <fieldset class="content-group margin-top-10">
                                <div class="form-group">
                                    {!! Form::label('content', "Treść:", null, ['class' => 'form-control']) !!}
                                    {!! Form::textarea('content', $page->content, ['class'=>'form-control', 'name' => 'content', 'id' => 'content'] ) !!}

                                </div>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


@endsection

@section('assets_end')
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('content', {height:"600px",extraPlugins:"forms"});
        $(document).on("click", "#create_modify", function () {
            $("#content").val(CKEDITOR.instances.content.getData());
        });
    </script>


@endsection
