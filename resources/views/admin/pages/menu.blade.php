@extends('admin.base')

@section('title')
Aktywne podstrony
@endsection

@section('assets_head')
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.js') }}"></script>
@endsection


@section('content')
    <div class="col-md-12">
        @if ($errors->any())
            @foreach($errors->all() as $error)
                <div style="color: red;">
                    {{ $error }}
                </div>
            @endforeach
        @elseif (isset($saved) && $saved == true)
            <div style="color: green;">
                Zapisano
            </div>
        @endif
    </div>
    <div class="col-md-12">
        <fieldset class="content-group margin-top-10">
            {!! Form::open(['route' => ['admin-pages-create'], 'method' => "POST"]) !!}
            <div class="form-group">
                {!! Form::label('name', "Tytuł:", null, ['class' => 'form-control']) !!}
                {!! Form::text('name', null, ['class'=>'form-control', 'name' => 'name', 'placeholder' => 'Wpisz nazwę'] ) !!}

            </div>
            <div class="form-group">
                {!! Form::submit('Dodaj nową zakładkę', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}

        </fieldset>
    </div>


    <div class="table-responsive">

        <table class="display datatable table table-stripped w-100" cellspacing="0" id="pages-menu-table">
            <thead>
            <tr>
                <th>LP</th>
                <th>ID</th>
                <th>Nazwa</th>
                <th>Szczegóły</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>LP</th>
                <th>ID</th>
                <th>Nazwa</th>
                <th>Szczegóły</th>
            </tr>
            </tfoot>
            <tbody>

            @foreach ($pages as $page)
                <tr>
                    <td>{{ $loop->index+1 }}</td>
                    <td>{{ $page->id_page }}</td>
                    <td>{{ $page->name }}</td>
                    <td>
                        <a class="btn btn-link" href="{{ route('page-pages', ['id_page' => $page->slug]) }}"><i class="fa fa-arrow-right"></i> </a>
                        <a class="btn btn-link" href="{{ route('admin-pages-page', ['id_page' => $page->id_page]) }}"><i class="fa fa-edit"></i> </a>
                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
@endsection

@section('assets_end')

    <script type="text/javascript">
        $(document).ready(function() {
            tables['pages-menu-table'] = $('#pages-menu-table').DataTable( {
                "lengthMenu": [[-1], ["Wszystkie"]],
                select: {
                    style: 'multi'
                }
            } );
        } );
    </script>
@endsection