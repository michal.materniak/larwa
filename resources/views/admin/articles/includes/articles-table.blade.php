<thead>
<tr>
    <th>LP</th>
    <th>ID</th>
    <th>Tytuł</th>
    <th>Autor</th>
    <th>Data utworzenia</th>
    <th>Szczegóły</th>
</tr>
</thead>
<tfoot>
<tr>
    <th>LP</th>
    <th>ID</th>
    <th>Tytuł</th>
    <th>Autor</th>
    <th>Data utworzenia</th>
    <th>Szczegóły</th>
</tr>
</tfoot>
<tbody>

@foreach ($articles as $article)
    <tr>
        <td>{{ $loop->index+1 }}</td>
        <td>{{ $article->id_article }}</td>
        <td>{{ $article->title }}</td>
        <td>{{ $article->author->firstname }} {{ $article->author->lastname }}</td>
        <td>{{ $article->created_at }}</td>
        <td>
            <a class="btn btn-link" href="{{ route('page-articles-article', ['slug' => $article->slug]) }}"><i class="fa fa-arrow-right"></i> </a>
            <a class="btn btn-link" href="{{ route('admin-articles-article', ['article' => $article->id_article]) }}"><i class="fa fa-edit"></i> </a>
        </td>
    </tr>
@endforeach
</tbody>