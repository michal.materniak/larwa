@extends('admin.base')

@section('title')
    Nieaktywne artykuły
@endsection

@section('assets_head')
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.js') }}"></script>
@endsection


@section('content')


    <div class="table-responsive">

        <table class="display datatable table table-stripped w-100" cellspacing="0" id="articles-menu-table">
            @include('admin.articles.includes.articles-table')
        </table>
    </div>
@endsection

@section('assets_end')

    <script type="text/javascript">
        $(document).ready(function() {
            tables['articles-menu-table'] = $('#articles-menu-table').DataTable( {
                "lengthMenu": [[-1], ["Wszystkie"]],
                select: {
                    style: 'multi'
                }
            } );
        } );
    </script>
@endsection