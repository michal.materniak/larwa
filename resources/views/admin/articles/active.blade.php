@extends('admin.base')

@section('title')
Aktywne artykuły
@endsection

@section('assets_head')
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.js') }}"></script>
@endsection


@section('content')

    <div class="col-md-12">
            @if ($errors->any())
                @foreach($errors->all() as $error)
                    <div style="color: red;">
                        {{ $error }}
                    </div>
                @endforeach
            @elseif (isset($saved) && $saved == true)
                <div style="color: green;">
                    Zapisano
                </div>
            @endif
    </div>
    <div class="col-md-12">
        <fieldset class="content-group margin-top-10">
            {!! Form::open(['route' => ['admin-articles-create'], 'method' => "POST"]) !!}
            <div class="form-group">
                {!! Form::label('title', "Tytuł:", null, ['class' => 'form-control']) !!}
                {!! Form::text('title', null, ['class'=>'form-control', 'name' => 'title', 'placeholder' => 'Wpisz tytuł'] ) !!}

            </div>
            <div class="form-group">
                {!! Form::submit('Dodaj nowy artykuł', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}

        </fieldset>
    </div>


    <div class="table-responsive">

        <table class="display datatable table table-stripped w-100" cellspacing="0" id="articles-menu-table">
            @include('admin.articles.includes.articles-table')
        </table>
    </div>
@endsection

@section('assets_end')

    <script type="text/javascript">
        $(document).ready(function() {
            tables['articles-menu-table'] = $('#articles-menu-table').DataTable( {
                "lengthMenu": [[-1], ["Wszystkie"]],
                select: {
                    style: 'multi'
                }
            } );
        } );
    </script>
@endsection