@extends('admin.base')

@section('title')
Panel Administracyjny
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
<pre>
Konto administracyjne:
login: admin.adminek@admin.pl
passw: admin1
wszystke konta poniżej mają dostęp tylko i wyłącznie uzytkownika:
@foreach(\App\User::limit(30)->offset(2)->get() as $user)
{{ $user->email }}
@endforeach
hasło "admin1"
emaile są generowane podczas seedowania.


MiniCMS posiada wszystkie wymagane moduły:
- moduł użytkownika (<a href="/login">logowanie</a>/<a href="/register">rejestracja</a>/<a href="/password/reset">reset hasła</a>)
    resetowanie hasła należy testować po uprzednim zarejestrowaniu emaila, na który zostanie wysłany link resetujący hasło.
- moduł bloga (zalogowany użytkownik może dodać posty i zostawić komentarze)
    komentarz mozna dodać pod każdym artykułem, tylko i wyłączenie kiedy user jest zalogowany
    wpis moze dodac kazdy user, jednak user moze przegladac, edytowac tylko i wylacznie swoje artykuły., admin rządzi wszystkim :)
- moduł dashboard (użytkownik może zobaczyć listę swoich komentarzy)
    user moze przegladac tylko swoje komentarze, tworzyc komentarze do artykułów. dashbord dla administratora jak na aplikacje mającą na
    Niektóre szablony oraz funkcje zostały skopiowane z innych aplikacji i dodane po odpowiednim przerobieniu do frameworka:
    admin moze wszystko, ponaddto co użytkownik:
        - tworzyc, dezaktywowac oraz tworzenie mechanizmu do generowania nagłóka, title oraz meta description dla zakładek na stronie
        - tworzyc, dezaktywowac oraz tworzenie mechanizmu do generowania nagłóka, title oraz meta description dla artykułów na stronie
        - tworzenie indywidualnych nagłówków, titlów oraz meta description, w razie potrzeby.
        - mialem jeszcze zrobić podstrony dla kategorii oraz zarzadzanie uzytkownikami, ale nie starczyło czasu

</pre>

        </div>
    </div>

@endsection