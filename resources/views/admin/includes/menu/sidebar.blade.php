<div id="sidebar-main" class="sidebar sidebar-default">
    <div class="sidebar-content">
        <ul class="navigation">

            <li class="navigation-header">
                <span>Main</span>
                <i class="icon-menu"></i>
            </li>
            <li>
                <a href="#"><i class="fa fa-home"></i> <span>Przejdź do serwisu</span></a>
                <ul style="display: block">
                    <li><a href="{{ route('page-homepage') }}" target="_blank">Strona Główna</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-homepage') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a>
            </li>
            @if (!Auth::guest() && Auth::user()->hasRole('Admin'))

            <li>
                <a href="#"><i class="fa fa-pencil"></i> <span>Serwis</span></a>
                <ul style="display: block">
                    <li><a href="{{ route('admin-settings-setting', ['id' => 1]) }}">Ustawienia</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-pencil"></i> <span>Strony</span></a>
                <ul style="display: block">
                    {{--<li><a href="{{ route('admin-pages-add') }}">Dodaj</a></li>--}}
                    <li><a href="{{ route('admin-pages-menu') }}">Menu</a></li>
                    <li><a href="{{ route('admin-pages-other') }}">Inne</a></li>
                    <li><a href="{{ route('admin-settings-setting', ['id' => 2]) }}">Ustawienia</a></li>
                </ul>
            </li>
            @endif
            <li>
                <a href="#"><i class="fa fa-pencil"></i> <span>Wpisy</span></a>
                <ul style="display: block">
                    <li><a href="{{ route('admin-articles-active') }}">Aktywne</a></li>
                    <li><a href="{{ route('admin-articles-unactive') }}">Nieaktywne</a></li>
                    @if (!Auth::guest() && Auth::user()->hasRole('Admin'))
                    <li><a href="{{ route('admin-settings-setting', ['id' => 3]) }}">Ustawienia</a></li>
                    @endif

                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-pencil"></i> <span>Komentarze</span></a>
                <ul style="display: block">
                    @if (!Auth::guest() && Auth::user()->hasRole('Admin'))
                    <li><a href="{{ route('admin-comments-show') }}">Wszystkie</a></li>
                    @endif
                    <li><a href="{{ route('admin-comments-own') }}">Moje</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>