<thead>
<tr>
    <th>LP</th>
    <th>ID</th>
    <th>Tytuł artykułu</th>
    <th>Pokaż</th>
    <th>Autor komentarza</th>
    <th>Data utworzenia</th>
</tr>
</thead>
<tfoot>
<tr>
    <th>LP</th>
    <th>ID</th>
    <th>Tytuł artykułu</th>
    <th>Pokaż</th>
    <th>Autor komentarza</th>
    <th>Data utworzenia</th>
</tr>
</tfoot>
<tbody>

@foreach ($comments as $comment)
    <tr>
        <td>{{ $loop->index+1 }}</td>
        <td>{{ $comment->id_comment }}</td>
        <td><a href="{{ route('page-articles-article', ['slug' => $comment->article->slug]) }}">{{ $comment->article->title }}</a> </td>
        <td><button class="btn btn-link showComment" value="{{ $comment->id_comment }}"><i class="fa fa-arrow-down"></i></button></td>
        <td>{{ $comment->author->firstname }} {{ $comment->author->lastname }}</td>
        <td>{{ $comment->created_at }}</td>
    </tr>
    <tr class="display-none" id="commentId_{{ $comment->id_comment }}">
        <td colspan="6" class="text-left">{{ $comment->content }}</td>
    </tr>
@endforeach
</tbody>
<script>
    $(document).ready(function () {
        $(document).on('click', '.showComment',function () {
            if($('#commentId_' + $(this).val()).hasClass('display-none'))
            {
                $('#commentId_' + $(this).val()).removeClass('display-none');
            }
            else
            {
                $('#commentId_' + $(this).val()).addClass('display-none');
            }
        });
    });
</script>