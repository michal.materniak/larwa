@extends('admin.base')

@section('title')
    Wszystkie komentarze
@endsection

@section('assets_head')
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/thirdparty/DataTables/jquery.dataTables.min.js') }}"></script>
@endsection


@section('content')


    <div class="row">
        <div class="col-md-12 ">
            <div class="table-responsive">
                <table class="display datatable table table-stripped w-100" cellspacing="0" id="comments-menu-table">
                    @include('admin.comments.includes.comments-table')
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            {{ $comments->render() }}
        </div>
    </div>
@endsection

@section('assets_end')

    <script type="text/javascript">
        $(document).ready(function() {
            tables['comments-menu-table'] = $('#comments-menu-table').DataTable( {
                "lengthMenu": [[-1], ["Wszystkie"]],
                select: {
                    style: 'multi'
                }
            } );
        } );
    </script>
@endsection