@extends('admin.base')

@section('title')
    Ustawienia dla zakładek
@endsection

@section('content')
    <div class="row margin-right-10">
        <div class="col-lg-12">
            <h4 class="card-title">{{ $setting->description }}</h4>

            <div id="message_ajax"></div>
            <fieldset class="content-group">

                <legend class="text-bold">Treść</legend>
                <div class="row ">

                    <div class="col-md-6">
                        {!! Form::model($setting, ['route' => ['admin-settings-update', $setting], 'method' => "PUT"]) !!}


                        @foreach ($setting->getSetting() as $setting_key => $setting_value)
                            @if(is_array($setting_value))

                                @foreach ($setting_value as $setting_key2 => $setting_value2)
                                        <div class="form-group row margin-top-10">

                                            <div class="col-md-4">
                                                {!! Form::label($setting_key2, $setting_key .'-'. $setting_key2.":", null, ['class' => 'form-control']) !!}
                                            </div>
                                            <div class="col-md-8">
                                                {!! Form::textarea($setting_key2, $setting->getSettingValueKey($setting_key, $setting_key2), ['class'=>'form-control', 'name' => 'setting-'.$setting_key.'-'.$setting_key2 ] ) !!}
                                            </div>
                                        </div>
                                @endforeach
                            @else
                                <div class="form-group row margin-top-10">

                                    <div class="col-md-4">
                                        {!! Form::label($setting_key, $setting_key.":", null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! Form::textarea($setting_key, $setting->getSettingKey($setting_key), ['class'=>'form-control', 'name' => 'setting-'.$setting_key ] ) !!}
                                    </div>
                                </div>
                            @endif

                        @endforeach
                        {{--<div class="form-group row margin-top-10">--}}

                            {{--<div class="col-md-4">--}}
                                {{--{!! Form::label('title', "Tytuł:", null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8">--}}
                                {{--{!! Form::textarea('title', $setting->getSettingKey('title'), ['class'=>'form-control', 'name' => 'setting-title' ] ) !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row margin-top-10">--}}
                            {{--<div class="col-md-4">--}}
                                {{--{!! Form::label('header', "Tytuł:", null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8">--}}
                                {{--{!! Form::textarea('header', $setting->getSettingKey('header'), ['class'=>'form-control', 'name' => 'setting-header' ] ) !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row margin-top-10">--}}
                            {{--<div class="col-md-4">--}}
                                {{--{!! Form::label('description', "Tytuł:", null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8">--}}
                                {{--{!! Form::textarea('description', $setting->getSettingKey('description'), ['class'=>'form-control', 'name' => 'setting-description' ] ) !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}

                    </div>

                    <div class="col-md-6">

                        <div class="row margin-top-10">
                            <div class="col-md-12">
                                {!! $setting->use !!}
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endsection