<!DOCTYPE HTML>
<html>

<head>
    <title>
        @yield('title') - {{ config('app.name') }}
    </title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('admin/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/assets/css/core.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/assets/icons/fontawesome/styles.min.css') }}">

    <script type="text/javascript" src="{{ asset('admin/lib/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin/assets/js/app.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/custom.js') }}"></script>
    @yield('assets_head')

</head>

<body>
@include('admin.includes.menu.navbar')
<div class="page-container">
    <div class="page-content">
        <!-- inject:SIDEBAR -->

        @include('admin.includes.menu.sidebar')

        <!-- inject:/SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="content-wrapper">
            <div class="content">
                @yield('content')
            </div>
        </div>
        <!-- /PAGE CONTENT -->
    </div>
</div>
@yield('assets_end')

</body>

</html>