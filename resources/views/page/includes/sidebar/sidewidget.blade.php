
<!-- Side Widget -->
<div class="card my-4">
    <h5 class="card-header">Side Widget</h5>
    <div class="card-body">
        {!! $globalSettings['sideWidget']['text'] !!}
    </div>
</div>
