
<!-- Categories Widget -->
<div class="card my-4">
    <h5 class="card-header">Kategorie</h5>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled mb-0">
                    @foreach($categories as $category)
                    <li>
                        <a href="{{ route('page-articles-category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>