@section('title')
    {!! $settingsSeo['title'] !!}
@endsection
@section('meta')

<meta content="{{ $settingsSeo['description'] }}" name="description">
@if($subpage->getCanonical() != null)
<link rel="canonical" href="{{ $subpage->getCanonical() }}"/>
@endif
{!! $subpage->getMeta() !!}
@endsection