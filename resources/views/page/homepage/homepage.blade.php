@extends('page.base')

@section('title')
    {!! $settingsSeo['title'] !!}
@endsection

@section('content')
    <h1>{!! $settingsSeo['header'] !!}</h1>
    <div>
        <div>
            {!! $page->content !!}
        </div>
        <div>
            @if (Auth::check())
                <a href="{{ route('admin-pages-page', ['id_page' => $page->id_page]) }}">Edytuj stronę</a>
            @endif
        </div>
    </div>
@endsection