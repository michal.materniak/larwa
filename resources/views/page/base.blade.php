<!DOCTYPE html>
<html lang="en">

<head>

    {!! $globalSettings['meta']['headBegin'] !!}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')
    <title>@yield('title')</title>
    <!-- Bootstrap core CSS -->
    {{--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{ asset('page/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    {{--<link href="css/blog-home.css" rel="stylesheet">--}}
    <link href="{{ asset('page/css/blog-home.css') }}" rel="stylesheet">
    {!! $globalSettings['meta']['headEnd'] !!}
</head>

<body>

{!! $globalSettings['meta']['bodyBegin'] !!}
@include('page.includes.menu')
<!-- Page Content -->
<div class="container">
    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
            @yield('content')
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            @include('page.includes.sidebar')
        </div>
        <!-- /.row -->
    </div>
</div>
    <!-- /.container -->
@include('page.includes.footer')
<!-- Bootstrap core JavaScript -->
    <script src="{{ asset('page/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('page/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

{!! $globalSettings['meta']['bodyEnd'] !!}
</body>

</html>
