@extends('page.base')

@section('title')
    Blog
@endsection

@section('content')

    <h1 class="my-4">Artykuły bloga
    </h1>
    <!-- Blog Post -->
    @foreach($articles as $article)
    <div class="card mb-4">
        <div class="card-body">
            <h2 class="card-title">{!! $article->title !!}</h2>
            <p class="card-text">{!!  $article->shortContent()  !!}</p>
            <a href="{{ route('page-articles-article', ['slug' => $article->slug]) }}" class="btn btn-primary">Czytaj więcej &rarr;</a>
        </div>
        <div class="card-footer text-muted">
            <div class="text-left">
            Napisany {{ date('d-m-Y H:i', strtotime($article->created_at)) }} przez {{ $article->author->firstname }} {{ $article->author->lastname }}
            </div>
            <div class="text-right">
            Kategoria: @if($article->category != null) {{ $article->category->name }} @else Brak kategorii @endif
            </div>
        </div>
    </div>
    @endforeach
    <!-- Blog Post -->

    <!-- Pagination -->

    <div class="row">
        <div class="col-md-12 ">
            {{ $articles->render() }}
        </div>
    </div>
    <!-- Pagination -->

@endsection