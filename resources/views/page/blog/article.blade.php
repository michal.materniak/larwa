@extends('page.base')

@include('page.includes.head.meta', ['subpage' => $article, 'settingSeo' => $settingsSeo])

@section('content')

    <!-- Blog Post -->
    <h1 class="my-4">{{ $settingsSeo['header'] }}</h1>
    <div>
        <h2 class="my-4">Kategoria: @if($article->category != null) {{ $article->category->name }} @else Brak kategorii @endif</h2>
        <h2 class="my-4">Autor: {{ $article->author->firstname . " " . $article->author->lastname }}</h2>
        <h2 class="my-4">Data utworzenia: {{ $article->created_at }}</h2>
        {!! $article->content !!}
    </div>
    <div>
        @if (Auth::check())
        <a href="{{ route('admin-articles-article', ['id_article' => $article->id_article]) }}">Edytuj stronę</a>
        @endif
    </div>
    <!-- /Blog Post -->
    <!-- Comments -->
    <hr>
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                @foreach($errors->all() as $error)
                    <div style="color: red;">
                        {{ $error }}
                    </div>
                @endforeach
            @elseif (isset($saved) && $saved == true)
                <div style="color: green;">
                    Dodano komentarz
                </div>
            @endif
        </div>
        @if(Auth::check())
            <div class="col-md-12">
                {{--{!! Form::model($article, ['route' => ['admin-comments-create', $article], 'method' => "POST"]) !!}--}}
                {!! Form::open(['route' => ['admin-comments-create', 'article' => $article], 'method' => "POST"]) !!}

                {!! Form::submit('Dodaj', ['class' => 'btn']) !!}
                <div class="form-group">
                    {!! Form::textarea('content', null, ['class'=>'form-control', 'name' => 'content', 'placeholder' => "Wpisz komentarz..." ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <div class="comments-list">
                @foreach($article->comments as $comment)
                <div class="media">
                    <p class="pull-right"><small>{{ $comment->daysAgo() }} dni temu </small> </p>
                    <div class="media-body">
                        <h4 class="media-heading user_name">{{ $comment->author->firstname }} {{ $comment->author->lastname }} </h4>
                        {{ $comment->content }}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Comments -->
@endsection