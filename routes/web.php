<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('logout',['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);

Route::group(['namespace' => 'Page'],function(){
    Route::get('/',['uses' => 'HomepageController@homepage', 'as' => 'page-homepage']);

    Route::get('/articles',['uses' => 'BlogController@list', 'as' => 'page-articles-list']);
    Route::get('/articles/{slug?}',['uses' => 'BlogController@article', 'as' => 'page-articles-article']);
    Route::get('/articles/category/{slug}',['uses' => 'BlogController@category', 'as' => 'page-articles-category']);

    Route::get('/pages/{page_slug}',['uses' => 'PagesController@page', 'as' => 'page-pages']);
});

Route::prefix('admin')->group(function(){
    Route::get('dashboard',['uses' => 'Admin\HomepageController@homepage', 'as' => 'admin-homepage']);

    Route::post('pages/create',['uses' => 'Admin\PagesController@create', 'as' => 'admin-pages-create','middleware' => 'roles', 'roles' => ['Admin']]);
    Route::get('pages/menu',['uses' => 'Admin\PagesController@menu', 'as' => 'admin-pages-menu','middleware' => 'roles', 'roles' => ['Admin']]);
    Route::get('pages/other',['uses' => 'Admin\PagesController@other', 'as' => 'admin-pages-other','middleware' => 'roles', 'roles' => ['Admin']]);
    Route::get('pages/{id_page}',['uses' => 'Admin\PagesController@page', 'as' => 'admin-pages-page','middleware' => 'roles', 'roles' => ['Admin']]);
    Route::put('pages/{id_page}',['uses' => 'Admin\PagesController@update', 'as' => 'admin-pages-update','middleware' => 'roles', 'roles' => ['Admin']]);

    Route::get('settings/{setting}',['uses' => 'Admin\SettingsController@setting', 'as' => 'admin-settings-setting','middleware' => 'roles', 'roles' => ['Admin']]);
    Route::put('settings/{setting}',['uses' => 'Admin\SettingsController@update', 'as' => 'admin-settings-update','middleware' => 'roles', 'roles' => ['Admin']]);

    Route::get('articles/{article?}',['uses' => 'Admin\ArticlesController@article', 'as' => 'admin-articles-article'])->where('article', '[0-9]+');
    Route::put('articles/{article?}',['uses' => 'Admin\ArticlesController@update', 'as' => 'admin-articles-update'])->where('article', '[0-9]+');
    Route::get('articles/active',['uses' => 'Admin\ArticlesController@active', 'as' => 'admin-articles-active']);
    Route::get('articles/unactive',['uses' => 'Admin\ArticlesController@unactive', 'as' => 'admin-articles-unactive']);
    Route::post('articles/create',['uses' => 'Admin\ArticlesController@create', 'as' => 'admin-articles-create']);

    Route::get('comments/show',['uses' => 'Admin\CommentsController@show', 'as' => 'admin-comments-show']);
    Route::get('comments/own',['uses' => 'Admin\CommentsController@own', 'as' => 'admin-comments-own']);
    Route::post('comments/create/{article}',['uses' => 'Admin\CommentsController@create', 'as' => 'admin-comments-create']);

});