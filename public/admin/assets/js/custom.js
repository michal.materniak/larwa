var tables = [];

function AJAXPOST(path, criteria)
{
    var requestData = create_request_data(criteria);
    // alert(JSON.stringify(requestData));
    return $.ajax({
        url: path,
        type : "POST",
        contentType : 'application/json',
        data : JSON.stringify(create_request_data(criteria)),
        success : function(result)
        {
            showInfoResponse(result);
        },
        error: function(xhr, resp, text)
        {
            showInfoResponse(xhr.responseText);
        }
    })
    ;
}

function AJAXGET(path) {
    return $.ajax({
        url: path,
        type : "GET",
        contentType : 'application/json',
        success : function(result)
        {
            showInfoResponse(result);
        },
        error: function(xhr, resp, text)
        {
            showInfoResponse(xhr.responseText.messageAjax);
        }
    })
    ;
}

function create_request_data(criteria)
{
    var requestData = {};
    if($.isArray(criteria))
    {
        $(criteria).each(function (i, key){
            requestData[key] = getCriteria(key);
        });
    }
    else
    {
        requestData = getCriteria(criteria);
    }
    return requestData;
}

function getCriteria(criteria)
{
    var requestData = {};

    $('.' + criteria).each(function (){

        if($(this).attr("name") !==  undefined)
        {
            if(($(this).attr("name") === "active") || $(this).attr("id") === "active" || $(this).attr("value") === "active")
            {
                if(($(this).attr("type") === "checkbox"))
                {
                    requestData[$(this).attr("name")] = $(this).prop('checked');
                }
                else
                {
                    requestData[$(this).attr("name")] = (($(this).val() === "true") || ($(this).val() === "1"));
                }

            }
            else if($(this).attr("type") === "checkbox")
            {
                requestData[$(this).attr("name")] = ($(this).is(':checked'));
            }
            else
            {
                requestData[$(this).attr("name")] = $(this).val();
            }
        }
    });
    return requestData;
}
function showInfoResponse(response)
{
    if(typeof response === 'object')
    {
        $.each(response, function (i, j) {

            if(i !== 'messageAjax')
            {
                $('#id' + i).val('').attr('placeholder', j);
            }
            else
            {
                $("#messageAjax").html(j).css({'color' : 'green'});
            }
        })
    }
    else if (typeof response === 'string')
    {
        $("#messageAjax").html(response).css({'color' : 'green'});
    }
}